import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { LocalStorageKeys } from 'src/app/shared/types';

@Injectable({
  providedIn: 'root',
})
export class StateService {
  language$ = new BehaviorSubject<string | null>(null);

  setLanguage(newLang: string): void {
    this.language$.next(newLang);
    localStorage.setItem(LocalStorageKeys.lang, newLang);
  }
}
