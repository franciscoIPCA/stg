import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HomeComponent } from './pages/home/home.component';
import { TranslateService } from 'src/app/core/translate.service';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home-routing.module';
import { HomeCoverComponent } from './components/home-cover/home-cover.component';
import { AboutUsComponent } from './components/about-us/about-us.component';

@NgModule({
  declarations: [HomeComponent, HomeCoverComponent, AboutUsComponent],
  imports: [
    CommonModule,
    TranslateService,
    HomeRoutingModule,
    ReactiveFormsModule,
  ],
})
export class HomeModule {}
