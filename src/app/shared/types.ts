export enum LocalStorageKeys {
  lang = 'language',
}

export enum Lang {
  pt = 'pt',
  en = 'en',
}
