import { Component } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  isMenuOpened: boolean = false;

  onOpenMenu(): void {
    const token = !!localStorage.getItem('token');
    const menuId = document.getElementById('hamburgerMenu');
    const idLogo = document.getElementById('logo');
    const idButtons = document.getElementById('header-buttons');
    const arrowSvgModalities = document.getElementById('arrow-svg-modalities');
    const arrowSvgServices = document.getElementById('arrow-svg-services');

    if (
      menuId?.classList.toString() === 'hamburger-button open position-fixed'
    ) {
      menuId?.classList.remove('open');
      this.isMenuOpened = false;
      menuId?.classList.remove('position-fixed');
      idLogo?.classList.remove('display-none');
      document.body.classList.remove('stop-scrolling');
      arrowSvgServices?.classList.remove('rotate90deg');
      arrowSvgModalities?.classList.remove('rotate90deg');

      if (!token) {
        idButtons?.classList.add('display-flex');
        idButtons?.classList.remove('display-none');
      } else if (token) {
        idButtons?.classList.remove('display-flex');
        idButtons?.classList.add('display-none');
      }
    } else {
      this.isMenuOpened = true;
      document.body.classList.add('stop-scrolling');
      menuId?.classList.add('open');
      menuId?.classList.add('position-fixed');
      idLogo?.classList.add('display-none');
      idButtons?.classList.remove('display-flex');
      idButtons?.classList.add('display-none');
      arrowSvgServices?.classList.remove('rotate90deg');
      arrowSvgModalities?.classList.remove('rotate90deg');
    }
  }
}
