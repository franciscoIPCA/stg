import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { StateService } from './core/state.service';
import { Lang, LocalStorageKeys } from './shared/types';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'oboe';

  constructor(
    private translateService: TranslateService,
    private stateService: StateService
  ) {}

  ngOnInit(): void {
    this.translateService.addLangs(Object.values(Lang));
    this.stateService.language$.subscribe((lang: string | null) => {
      if (!!lang)
        this.translateService.use(lang),
          localStorage.setItem(LocalStorageKeys.lang, lang);
      else if (!!localStorage.getItem(LocalStorageKeys.lang))
        this.translateService.use(localStorage.getItem(LocalStorageKeys.lang)!);
      else
        this.translateService.use(this.translateService.defaultLang),
          localStorage.setItem(
            LocalStorageKeys.lang,
            this.translateService.defaultLang
          );
    });
  }
}
